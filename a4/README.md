# LIS4368 - Advanced Web App Development 

## Sydney Sawyer

### Assignment 4 Requirements:

*Deliverables:*

1. Review subdirectories and files
    - Each assignment must have its own global subdirectory
2. Add server-side validation
3. Compile servlet files
4. Research regular expressions 
5. Backward-engineer skillsets 10-12
6. Extra Credit: Individuals providing server-side validation identical to the client-side validation requirements of project 1 will recieve 20 additional points. 


#### README.md file should include the following items:

* Screenshot of failed validation 
* Screenshot of passed validation
* Screenshot of Skillsets 10-12 

>#### A4 MVC Notes: 
>
> **Model View Controller (MCV)** is a framework that dividesan application's implementation into three discrete component roles: model, views, and controllers. 
>
> Moreover, in computer scienc and information technology, separation of concerns (SoC) is a design principle for separating a computer program into distinct sections (or layers), with each section addressing a seperate concern: e.g., presentation layer, business logic layer, data access layer, and persistence layer.
>
> **Model:** The "business layer". 
> How do we represent the data?
> Defines business objects, and provides methods for business processing (e.g., Customer and CustomerDB classes).
>
> **Controller:** Directs the application "flow" - using servlets.
> Generally, reads parameters sent from requests, updates the model, saves data to the data store, and forwards updated model to JSPs for presentation (e.g., CustomerServlet).
>
> **View:** How do I render the result? 
> Defines the presentation - using .html or .jsp files (e.g., index.html, index.jsp, customerform.jsp, thanks.jsp)
>
>


#### Assignment 4 Screenshots:

**Failed Validation**:

![Failed Validation](img/formfailure.png)

**Passed Validation**:

![Passed Validation](img/formvalidation.png)

**Skillsets 10 & 11**

|*10*:  |*11*:  |
|-|-|
|![SS10](img/skillset10.png)  |![SS11](img/skillset11.png)  |


**Skillset 12**:

|*A*: |*B*: 	|*C*    |
|:-:	|:-:	|:-:    |
|![SS12A](img/skillset12_a.png) 	|![SS12B](img/skillset12_b.png) 	|![SS12C](img/skillset12_c.png)  |


#### Links:

*Back to Main README - lis4368*:
[Main README](https://bitbucket.org/sls16d/lis4368/src/master/ "Link")