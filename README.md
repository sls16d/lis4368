# LIS 4368 - Advanced Web Applications Development LIS 4368

## Sydney Sawyer

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations 
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git commands with descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Login to MySQL through Ampps
    - Create ebookshop database
    - Write a Database servlet
    - Deploy a servlet using @WebServlet
    - Compile servlet files
    - Provide assessment links and screenshots
3. [A3 README.MD](a3/README.md "My A3 README.md file")
    - Create a data repository 
    - Follow business rules 
    - Save SQL code and ERD model
    - Forward-engineer to your local repository
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create a server-side validation form 
        - Use your servlet-api.jar path
    - Compile servlet files 
    - Backward-engineer skillsets 10-12
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Include server-side validation form from A4
    - Compile class and servlet files
    - Connect form with local SQL Server
    - Add customer list data to customer table 

### Project Requirements: 

1. [P1 README.MD](p1/README.md "My P1 README.md file")
    - Create a Validation form connected to the customer entity in database
    - Add form controls to match attributes of the customer entity
    - Add jQuery validation and regular expressions as per the entity attribute requirements 
2. [P2 README.MD](p2/README.md "My P2 README.md file")
    - Create a Valiation form connected to the customer entity in database
    - Use MCV frameworks
    - Use prepared statements to prevent SQL injection
    - Make form complete CRUD functionality