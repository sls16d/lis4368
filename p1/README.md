# LIS4368 - Advanced Web App Development 

## Sydney Sawyer

### Project 1 Requirements:

*Deliverables:*

1. Review subdirectories and files
    - Each assignment must have its own global subdirectory
2. Add form controls to match the customer entity
3. Add jQuery validation and regular expressions to index.jsp
4. Research validation codes 
5. Backward-engineer the Grade App skillset


#### README.md file should include the following items:

* Screenshot of local web app home page
* Screenshot of failed validation 
* Screenshot of passed validation
* Screenshot of Grade App skillset 

>#### P1 Validation Form Requirements: 
>
> a. Suitable modify meta tags 
>
> b. Change title, navigation links, and header tags appropriately 
>
> c. Add form controls to match the attributes of __customer__ table
>
> d. __Add the following jQuery validation and regular expressions-- as per the entity attribute requirements (and screenshots below):
>
>> i. *All* input fields, except Notes __are required__
>>
>> ii. Use min/max jQuery validation
>>
>> iii. Use regexp to only allow appropriate characters for each control:
>>
>>> cus_fname, cus_lname, cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email, cus_balance, cus_total_sales, cus_notes
>>>
>>> __fname, lname__: provided
>>>
>>> __street, city__: 
>>>
>>> - no more than 30 characters 
>>>
>>> - Street: must only contain letters, numbers, commas, hyphens, or periods
>>>
>>> - City: can only contain letters, numbers, hyphens, and space character (29 Palms)
>>>
>>> __state__:
>>>
>>> - must be 2 characters
>>>
>>> - must only contain letters
>>>
>>> __zip__:
>>>
>>> - must be between 5 and 9 characters, inclusive
>>>  
>>> - must only contain numbers
>>>
>>> __phone__:
>>>
>>> - must be 10 characters, including area code
>>>
>>> __email__: provided (also, see below: Lesson 7)
>>>
>>> __balance, total_sales__:
>>> 
>>> - no more than 6 digits, including decimal point 
>>>
>>> - must only contain numbers, and decimal point (if used)
>>>
>>
>>iv. **Ater** testing jQuery validation, use HTML5 property to limit the number of characters for each control
>>
>>v. __Reasearch what the following validation code does__:
>>>
>>> valid: 'fa fa-check',
>>> 
>>> invalid: 'fa fa-times',
>>>
>>> validating: 'fa fa-refresh'
>>
>


#### Project 1 Screenshots:

**Local Web App Home Page Slides**:

![Home Page Slide 1](img/localhost1.png)
![Home Page Slide 2](img/localhost2.png)
![Home Page Slide 3](img/localhost3.png)

**Failed Validation**:

![Failed Validation](img/failed_validation.png)

**Passed Validation**:

![Passed Validation](img/passed_validation.png)

**Grade App Skillset**:

![Grade App](img/GradeApp.png)


#### Links:

*Back to Main README - lis4368*:
[Main README](https://bitbucket.org/sls16d/lis4368/src/master/ "Link")