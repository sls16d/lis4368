# LIS4368 - Advanced Web App Development 

## Sydney Sawyer

### Assignment 5 Requirements:

*Deliverables:*

1. Review subdirectories and files
    - Especially META-INF and WEB-INF
    - Each assignment must have its own global subdirectory
2. Include server-side validation from A4
3. Compile class and servlet files 
5. Backward-engineer skillsets 13-15


#### README.md file should include the following items:

* Screenshot of pre-valid user form entry
* Screenshot of post-valid user form entry
* Screenshot of MySQL customer table entry 
* Screenshot of Skillsets 13-15 

>#### A5 Notes: 
>
> Along with the **MVC** framework -- that is, a framework that divides an application's implementation into three component roles: models, views, and controllers (See A4 Requirements)-- programmers and web developers must be able to design and deploy data-driven applications.
> When doing so, the acronym CRUD is used, which generally means create, read, update, and delete -- sometimes, including SCRUD with an "S" for search.
> These are the four basic functions of persistent storage
>



#### Assignment 5 Screenshots:

**Pre-valid user form entry**:

![Pre validation form](img/prevalid.png)

**Post-valid user form entry**:

![post validation form](img/postvalid.png)

**Associated database entry**:

![Database entry](img/sql.png)

**Skillset 13**:

![SS13](img/ss13.png)

**Skillset 14**:

![SS14](img/ss14.png)

**Skillset 15**:

![SS15](img/ss15.png)

#### Links:

*Back to Main README - lis4368*:
[Main README](https://bitbucket.org/sls16d/lis4368/src/master/ "Link")