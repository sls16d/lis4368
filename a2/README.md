# LIS4368 - Advanced Web App Development 

## Sydney Sawyer

### Assignment 2 Requirements:

*Three Parts:*

1. Compiling servlet files
2. Write a Database servlet
3. Create a database and connect 


#### README.md file should include the following items:

* Assessment links
* Screenshot of query results
* Screenshots of local web app 


#### Assignment Screenshots:

**http://localhost:9999/hello with no index.html**:

![no index.html](img/localhost_hello.png)

![hellohome.html](img/localhost_HelloHome.png)

**http://localhost:9999/hello using index.html**:

![using index.html](img/localhost_index.png)

**http://localhost:9999/hello/sayhello**:

![Say Hello](img/sayhello.png)

**http://localhost:9999/hello/querybook.html**:

![Querybook Page](img/querybook.png)

**http://localhost:9999/hello/query results page**:

![Query Results](img/query_results.png)

#### Local Web App Screenshots:

![local web app 1](img/localweb1.png)

![local web app 2](img/localweb2.png)


#### Tutorial Links:

*Back to Main README - lis4368*:
[lis4368](https://bitbucket.org/sls16d/lis4368/src/master/ "Link")