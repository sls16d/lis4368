# LIS4368 - Advanced Web App Development 

## Sydney Sawyer

### Assignment 3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Provide bitbucket read-only access
4. Include links to:
    - a3.mwb
    - a3.sql


#### README.md file should include the following items:

* Screenshot of A3 ERD
* Screenshot of select statements
* Screenshots of local web app 
* A3 .mwb and .sql links

>#### A3 Business Rules: 
>
> A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:
>
>> 1. A customer can buy many pets, but each pet, if purchased by only one customer.
>> 2. A store has many pets, but each pet is sold by only one store. 
>
> *Remember*: an organization's business rules are the key to a well-designed database.
>
> For the Pet's R-Us business, it's important to ask the following questions to get a better idea of how the database and Web application should work together: 
>
>> - Can a customer exist without a pet? Seems reasonable. Yes. (optional)
>> - Can a pet exist without a customer? Again, yes. (optional)
>> - Can a pet store not have any pets? It wouldn't be a pet store. (mandatory)
>> - Can a pet exist without a pet store? Not in this design. (mandatory)
>
> The following criteria are required for the submitted ERD: 
>
>> - Connectivities
>> - Cardinalities 
>> - Relationship strengths
>> - Relationship participations
>> - PKs, FKs, PFs (if used)
>> - Attributes
>> - Relationships between entities
>


#### Assignment Screenshots:

**A3 ERD**:

![A3 ERD](img/a3_erd.png)


**Populated pet table**:

![Pet Table](img/pet_result.png)

**Populated customer table**:

![Customer Table](img/customer_result.png)

**Populated petstore table**:

![Petstore Table](img/petstore_result.png)

#### Local Web App Screenshot:

![local web app a3](img/localhost.png)

#### Links:

*MWB Link*:
[a3.mwb](docs/a3.mwb "Link")

*SQL Link*:
[a3.sql](docs/a3.sql "Link")

*Back to Main README - lis4368*:
[Main README](https://bitbucket.org/sls16d/lis4368/src/master/ "Link")