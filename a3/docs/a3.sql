-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sls16d
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sls16d` ;

-- -----------------------------------------------------
-- Schema sls16d
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sls16d` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `sls16d` ;

-- -----------------------------------------------------
-- Table `sls16d`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sls16d`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `sls16d`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`),
  UNIQUE INDEX `pet_id_UNIQUE` (`pst_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `sls16d`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sls16d`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `sls16d`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_tot_sales` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`),
  UNIQUE INDEX `cus_id_UNIQUE` (`cus_id` ASC))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `sls16d`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sls16d`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `sls16d`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL COMMENT 'age is calculated in months\n',
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  UNIQUE INDEX `pet_id_UNIQUE` (`pet_id` ASC),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `sls16d`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `sls16d`.`customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `sls16d`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `sls16d`;
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'PetSmart', '650 Ponce de Leon Ave NE', 'Atlanta', 'GA', 30308, 4048272363, 'petsmart@gmail.com', 'petsmart.com', 800000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Petco', '2020 Howell Mill Rd NW', 'Atlanta ', 'GA', 30318, 4043515591, 'petco@gmail.com', 'petco.com', 900000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Park Pet Supply', '479 Flat Shoals Ave SE', 'Atlanta', 'GA', 30316, 4045880140, 'parkpetsupply@gmail.com', 'parkpetsupply.com', 100000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'The Pet Set', '975 Piedmont Ave NE', 'Atlanta', 'GA', 30309, 4042496668, 'petset@gmail.com', 'petset.com', 150000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Gallery', '1465 Chattahoochee Ave NW', 'Atlanta', 'GA', 30318, 4043517490, 'petgallery@gmail.com', 'petgallery.com', 50000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Highland Pet Supply', '1186 N Highland Ave NE', 'Atlanta', 'GA', 30306, 4048925900, 'h.petsupply@gmail.com', 'petsupply.co,', 20000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Inman Park Pet Works', '914 Austin Ave NE', 'Atlanta', 'GA', 30307, 4045224544, 'ipark.petworks@gmail.com', 'petworks.com', 500000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Supermarket', '1544 Piedmont Ave NE', 'Atlanta', 'GA', 30324, 4048790580, 'psupermarket@gmail.com', 'petsupermarket.com', 112000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'The Whole Dog Market', '985 Monroe Dr NE', 'Atlanta', 'GA', 30308, 4045492727, 'wholedogmarket@gmail.com', 'dogmarket.com', 15000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Intown Healthy Hound', '891 Cherokee Ave SE', 'Atlanta', 'GA', 30315, 4042283898, 'intown.hh@gmail.com', 'healthyhound.com', 30000.00, NULL);
INSERT INTO `sls16d`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Playhouse', '1998 Hosea L Williams Dr NE', 'Atlanta', 'GA', 30317, 4043782829, 'pet.playhouse@gmail.com', 'petplayhouse.com', 99000.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `sls16d`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `sls16d`;
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Malcom', 'Cox', '1849 Post Farm Road', 'Atlanta', 'GA', 30305, 4042649081, 'm.cox@gmail.com', 12.00, 24.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Velma', 'Sharp', '1581 Stroop Hill Road', 'Atlanta', 'GA', 30309, 4048700547, 'v.sharp@gmail.com', 6.00, 99.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Pat', 'Stevens', '1306 Kuhl Ave', 'Atlanta', 'GA', 30308, 4045276177, 'p.stevens@gmail.com', 81.00, 300.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Jan', 'Schwartz', '958 Hanifan Ln', 'Atlanta', 'GA', 30308, 6788581875, 'janS@gmail.com', 5.00, 5.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Johnnie', 'Mason', '2587 Limer Street', 'Atlanta', 'GA', 30303, 4044271361, 'jmason@gmail.com', 18.00, 37.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Clinton', 'Cain', '4348 Davis Street', 'Atlanta', 'GA', 30305, 7062483943, 'ccain@gmail.com', 39.00, 68.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Sabrina', 'Warner', '1907 Clement Street', 'Atlanta', 'GA', 30303, 4044196173, 'sabwarner12@gmail.com', 2.00, 85.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Cameron', 'Guzman', '22 Course St.', 'Atlanta', 'GA', 30309, 6783628920, 'cam.guz@gmail.com', 33.00, 33.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Rachel', 'Start', '3234 Pine Garden Ln', 'Atlanta', 'GA', 30305, 8322029140, 'rach.start@gmail.com', 23.00, 27.00, NULL);
INSERT INTO `sls16d`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_tot_sales`, `cus_notes`) VALUES (DEFAULT, 'Kyle', 'McDermott', '997 Duck Dr.', 'Atlanta', 'GA', 30304, 4042912903, 'kylemcd@gmail.com', 120.00, 440.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `sls16d`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `sls16d`;
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'dog', 'm', 20.00, 300.00, 12, 'tan', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'cat', 'f', 18.00, 55.00, 24, 'white', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'rabbit', 'm', 10.00, 45.00, 36, 'tan', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 4, 'hamster', 'f', 5.00, 15.00, 5, 'black', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 5, 'bird', 'm', 13.00, 24.00, 24, 'green', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 6, 'ferret', 'f', 8.00, 20.00, 36, 'brown', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 7, 'guinea pig', 'm', 6.00, 20.00, 12, 'orange', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 8, 'snake', 'f', 22.00, 42.00, 24, 'silver', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 9, 'lizard', 'm', 12.00, 34.00, 36, 'white', NULL, 'y', 'y', NULL);
INSERT INTO `sls16d`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 10, 'spider', 'f', 12.00, 34.00, 1, 'black', NULL, 'y', 'y', NULL);

COMMIT;

