# LIS4368 - Advanced Web App Development 

## Sydney Sawyer

### Project 2 Requirements:

*Deliverables:*

1. Review subdirectories and files
    - Especially META-INF and WEB-INF
    - Each assignment must have its own global subdirectory
2. Include server-side validation from A4
3. Compile class and servlet files 



#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of passed validation
* Screenshot of display data 
* Screenshot of modify form
* Screenshot of modified data
* Screenshot of delete warning
* Screenshot of associated database changes  

>#### Project 2 Notes: 
>
> These requirements complete the JSP/Servlets web application using the MVC framework and providing *create*, *read*, *update*, and *delete* (CRUD) functionality-- that is, the four basic functions of persistent storage.
> In addition a search (SCRUD) option could be added, and which will be left as a research exercise.
> 
>



#### Project 2 Screenshots:

**Valid User Form Entry (customerform.jsp)**:

![Validation user form](img/valid_form.png)

**Passed Validation (thanks.jsp)**:

![Passed validation](img/passed_validation.png)

**Display Data (customers.jsp)**:

![Display data](img/display_data.png)

**Modify Form (modify.jsp)**:

![Modify form](img/modify_form.png)

**Modified Data (customers.jsp)**:

![Modified data](img/modified.png)

**Delete Warning (customers.jsp)**:

![Delete warning](img/delete.png)

**Associated Database Changes (Select, Insert, Update, Delete)**:

![Associated database changes](img/data1.png)
![Associated database changes](img/data2.png)

#### Links:

*Back to Main README - lis4368*:
[Main README](https://bitbucket.org/sls16d/lis4368/src/master/ "Link")