import java.util.Scanner;

import javax.lang.model.util.ElementScanner14; 
public class CountCharacters 
{
    public static void main(String[] args)
    {
        System.out.println("Developer: Sydney Sawyer");
        System.out.println("Program counts number and types of characters: that is, lettersm spaces, numbers, and other characters.");
        System.out.println("Hint: You may find the following methods helpful: isLetter(), isDigit(), isSpaceChar().");
        System.out.println("Additionally, you could add the functionality of testing for upper vs lower case letters.");
        System.out.println();

        int letter = 0;
        int space = 0;
        int num = 0;
        int other = 0;
        String str = "";


        System.out.print("Please enter string: ");
            Scanner input = new Scanner(System.in);
            str = input.nextLine();

            char[] ch = str.toCharArray();

            for(int i = 0; i < str.length(); i++)
            {
                if(Character.isLetter(ch[i]))
                {
                    letter ++;
                }
                else if(Character.isDigit(ch[i]))
                {
                    num ++;
                }
                else if(Character.isSpaceChar(ch[i]))
                {
                    space ++;
                }
                else 
                {
                    other ++;
                }
            }
            System.out.println("\nYour string: \"" + str + "\"has the following number and types of characters:");
            System.out.println("letter(s): " + letter);
            System.out.println("space(s): " + space);
            System.out.println("number(s): " + num);
            System.out.println("other character(s): " + other);
    }
}