import java.util.Scanner;



public class GradeApp {
    public static void main(String[] args) {  // Count the number of correct answers
        int count = 0;
        int realCount; // Count the number of questions
        double sum = 0;
        double usrGrade = 0;
        double usrAvg = 0;
        double realSum = 0;
        
        Scanner input = new Scanner(System.in);

        System.out.println("\nDeveloped by Sydney Sawyer\n");
        System.out.println("Grade average and total is rounded to 2 decimal places.\n");
        System.out.println("Note: Program does *not* check for non-numeric characters.\n");
        System.out.println("To end program enter -1.\n");

        while (usrGrade != -1) {

            System.out.print("Enter grade: ");
            usrGrade = input.nextDouble();

            if (usrGrade < -1 || usrGrade > 100) {
                System.out.println("Invalid entry, not counted.");
                count--;
            }
            else
                sum += usrGrade;
                count++;

        }
        
        realCount = count - 1;
        realSum = sum + 1.00;
        usrAvg = realSum / realCount;

        System.out.println("\nGrade count: " + realCount);
        System.out.format("Grade total: %.2f\n", realSum);
        System.out.format("Grade average: %.2f\n", usrAvg); 


    }

}
