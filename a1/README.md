# LIS4368 - Advanced Web Applications Development

## Sydney Sawyer

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation [http://localhost:9999/lis4368/a1/index.jsp](http://localhost:9999/a1/lis4368/index.jsp "Local web app")
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1)
* Screenshot of running http://localhost:9999 (#2)
* git commands w/ short descriptions 
* Bitbucket repo links: a) this assignment and b) the completed tutorial (bitbuckestationlocations)

#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git merge - Join two or more development histories together

#### Assignment Screenshots:

*Screenshot of running Java Hello*:

![Java Hello](img/javaHello.png)

*Screenshot of running http://localhost:9999*:

![localhost:9999](img/tomcat_running.png)

*Local web app screenshot*:

![local web app](img/local_host.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations*:
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/sls16d/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*This Assignment - lis4368*:
[A1 for lis4368](https://bitbucket.org/sls16d/lis4368/src/master/ "A1 Link")